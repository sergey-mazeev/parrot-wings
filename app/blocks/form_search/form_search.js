window.addEventListener('load', function () {

    // hide search on media size screen

    const submitSearch = document.querySelector('.content__search input[type="submit"]');

    submitSearch.addEventListener('click', function (event) {
        if (window.matchMedia('(max-width: 1024px) and (min-width: 690px)').matches) {
            const formSearch = this.parentElement;
            const inputSearch = this.previousElementSibling;
            if (!formSearch.classList.contains('form_search-visible')) {
                formSearch.classList.add('form_search-visible');
                event.preventDefault();
            } else if (inputSearch.value.trim() === '' || inputSearch.value === undefined) {
                formSearch.classList.remove('form_search-visible');
                event.preventDefault();
            }
        }
    });

    // ! hide search on media size screen

});