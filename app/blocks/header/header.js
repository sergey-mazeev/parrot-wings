window.addEventListener('load', function () {

    // fix header on scroll
    const nav = document.querySelector('.header__nav');
    const initialNavTopOffset = nav.getBoundingClientRect().top + document.documentElement.scrollTop;

    const fixHeader = () => {
        const scrollPosition = document.documentElement.scrollTop;
        if (scrollPosition > initialNavTopOffset && window.matchMedia('(min-width: 767px)').matches) {
            nav.classList.add('header__nav_fixed');
        } else {
            nav.classList.remove('header__nav_fixed');
        }
    };

    fixHeader();
    document.addEventListener('scroll', fixHeader);
    // ! fix header on scroll


    // add burger and search icon
    let mobileControlsFlag = false;

    const createMobileControls = (callback) => {
        if (window.matchMedia('(max-width: 690px)').matches && !mobileControlsFlag) {
            const mobileControls = document.createElement('div');
            mobileControls.className = 'header__mobile-controls';
            mobileControls.innerHTML = `<button id="header__burger" class="header__burger">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </button>
                                        <button id="header__search" class="header__search"></button>`;
            document.querySelector('.header').appendChild(mobileControls);
            callback();
            mobileControlsFlag = true;
        }
    };
    // ! add burger and search icon

    // move title to header on mobile devices
    let titleMoveFlag = false;
    const moveTitle = () => {
        const pageTitle = document.querySelector('.content__title');
        if (pageTitle && window.matchMedia('(max-width: 690px)').matches && !titleMoveFlag) {
            document.querySelector('.header').appendChild(pageTitle);
            titleMoveFlag = true;
        } else if (pageTitle && window.matchMedia('(min-width: 691px)').matches && titleMoveFlag) {
            document.querySelector('.content__header').insertBefore(pageTitle, document.querySelector('.content__search'));
            titleMoveFlag = false;
        }
    };
    moveTitle();
    window.addEventListener('resize', moveTitle);
    // ! move title to header on mobile devices

    // show mobile menu
    const body = document.querySelector('body');

    const showMobileElems = () => {
        const burger = document.getElementById('header__burger');
        const body = document.querySelector('body');
        const search = document.getElementById('header__search');

        const toggleMobileMenu = () => {
            burger.classList.toggle('header__burger_close');
            body.classList.toggle('body_w-menu');
        };

        burger.addEventListener('click', function (event) {
            toggleMobileMenu();
            event.preventDefault();
        });
        // ! show mobile menu
        // show mobile search
        search.addEventListener('click', function (event) {
            body.classList.toggle('body_w-search');
            event.preventDefault();
        });
        // ! show mobile search
    };


    // normalize body
    const normalizeBody = () => {
        if (window.matchMedia('(min-width: 690px)').matches) {
            if (body.classList.contains('body_w-search')) {
                body.classList.remove('body_w-search');
            }
            if (body.classList.contains('body_w-menu')) {
                body.classList.remove('body_w-menu');
            }
        }
    };

    window.addEventListener('resize', normalizeBody);
    // ! normalize body


    createMobileControls(showMobileElems);
    window.addEventListener('resize', function () {
        createMobileControls(showMobileElems);
    });

});