(function ($) {
  //region Маски
  let tel_default_flag = true;
  const tel_default_options = {
    onKeyPress: function (cep, event, currentField, options) {
      if (cep === "+7 8"
        && tel_default_flag) {
        currentField.val('+7 ');
        tel_default_flag = false;
      }
    }
  };
  $('input[type="tel"]').mask('+7 999 999-99-99', tel_default_options);
  //endregion

  //region resizeEnd
  $(window).resize(function () {
    if (this.resizeTO) {
      clearTimeout(this.resizeTO);
    }
    this.resizeTO = setTimeout(function () {
      $(this).trigger('resizeEnd');
    }, 200);
  });
  //endregion
})(jQuery);